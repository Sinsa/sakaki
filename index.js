const express = require("express");
const app = express();
const Config = require("./config.json");
const fs = require("fs");

const port = Config.port ?? 3000;

for (const file of Config.files) {
    app.get(`/files/${file.path}`, (request, response) => {
        console.log(`Serving request /files/${file.path}`);
        if (fs.existsSync(file.location)) {
            response.sendFile(file.location);
        } else {
            response.sendStatus(404);
        }
    });
}

app.get('/', (request, response) => {
    response.send('This is Sakaki, a static file host by Sinsa.');
});

app.listen(port, () => console.log(`Listening on port ${port}`));
